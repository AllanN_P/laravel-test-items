<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class HomeController extends Controller
{

    public function show(Request $request)
    {
        $dataJson = $this->getJSON();

        $data = $this->setCurrentValue($dataJson);

        if($request->ajax()){
            return view('welcome', $data);
        }

        else
        {
            return view('table', $data);
        }

    }

    /*
     * Функция получения JSON
     */

    private function getJSON()
    {
        $postData = file_get_contents('http://phisix-api3.appspot.com/stocks.json');
        $data = json_decode($postData, true);
        return $data;
    }

    /*
     * Функция корректировки данных
     */

    private function setCurrentValue($values)
    {
        foreach ($values['stock'] as &$value)
        {
            $value['volume'] = intval ($value['volume']);

            $value['price']['amount'] = number_format($value['price']['amount'], 2, '.', '');
        }

        return $values;
    }

}
