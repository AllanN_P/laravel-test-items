<tbody>
<tr>
    <th>Name</th>
    <th>Volume</th>
    <th>Amount</th>
</tr>
@foreach($stock as $value)
    <tr>
        <td>{{ $value['name'] }}</td>
        <td>{{ $value['volume'] }}</td>
        <td>{{ $value['price']['amount'] }}</td>
    </tr>
@endforeach
</tbody>
