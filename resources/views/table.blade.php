<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="https://getbootstrap.com/docs/3.3/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="col-sm-12">
    <span id="up" onclick="up()" class="btn btn-info "> <i class="glyphicon glyphicon-refresh"></i></span>
</div>
<table id="results" class="col-sm-12">
    <tbody>
    <tr>
        <th>Name</th>
        <th>Volume</th>
        <th>Amount</th>
    </tr>
    @foreach($stock as $value)
        <tr>
            <td>{{ $value['name'] }}</td>
            <td>{{ $value['volume'] }}</td>
            <td>{{ $value['price']['amount'] }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
<script>
    function up(){
    $.ajax({
        url: "/",
        cache: false
    }).done(function( html ) {
        $("#results").append(html);
    });
    }
    setInterval("up()", 15000);

</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://getbootstrap.com/docs/3.3/dist/js/bootstrap.min.js"></script>
</body>
</html>